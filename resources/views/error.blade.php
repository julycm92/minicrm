@extends('layouts.app')
@section('content')
    <div>
        <div class="panel panel-primary">
            <div class="panel-heading">{{$titulo}}</div>
            <div class="panel-body">
                {{$mensaje}}
            </div>
        </div>
    </div>
@endsection

