@extends('layouts.app')

@section('content')
    @include('formUsuario',['nombre'=>'Nuevo usuario',
                            'accion'=>url('usuarios'),
                            'submitNombre'=>'Registrar',
                            'usuario'=> new \App\Usuario()])
@stop
