@extends('layouts.body')

@section('content')
    @include('usuarios.formUsuario',['nombre_panel'=>'Edit',
                            'nombre' => 'Usuarios',
                            'accion'=>url('usuarios'),
                            'submitNombre'=>'Registrar',
                            'usuario'=> $usuario])
@stop
