@extends('layouts.body')

@section('content')
    @include('usuarios.formUsuario',['nombre_panel'=>'Alta',
                            'nombre' => 'Usuarios',
                            'accion'=>url('usuarios'),
                            'submitNombre'=>'Registrar',
                            'usuario'=> new \App\Usuario()])

@stop
