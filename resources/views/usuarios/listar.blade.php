@extends('layouts.body')

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Tables</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    DataTables Advanced Tables
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <table sid="usuarios" width="100%" class="table table-striped table-bordered table-hover"
                           id="dataTables-example">
                        <thead>
                        <tr>
                            <th data-priority="0">Nombre</th>
                            <th data-priority="1">Email</th>
                            <th data-priority="2">Telefono</th>
                        </tr>
                        </thead>
                        <thead id="inputsBuscar">
                        <tr>
                            <th></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($usuarios as $usuario)
                            <tr>
                                <td>{{$usuario->name}}</td>
                                <td>{{$usuario->email}}</td>
                                <td>{{$usuario->telefono}}</td>
                                <td class="columnaBoton"><a href="/usuarios/{{ $usuario['id'] }}/edit"
                                                            class="btn btn-warning btn-sm"><span
                                                class="glyphicon glyphicon-edit"> Editar</span></a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    @include('componentes.scriptDatatable',['tabla'=>'usuarios'])

@stop

@section('javascript')
    <!-- DataTables JavaScript -->
    <script src="/template/vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="/template/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="/template/vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="/template/dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
        $(document).ready(function() {
            $('#dataTables-example').DataTable({
                responsive: true
            });
        });
    </script>

@stop
