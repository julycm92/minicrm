<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">{{$nombre}}</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                {{$nombre_panel}}
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-12">
                       <form  role="form" class="form-horizontal" method="POST" action="{{$accion}}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        @include('componentes.cambiosGuardados')

                        @include('componentes.input',['name'=>'name',
                        'nombreMostrar'=>'Nombre:',
                        'editar'=>$usuario->name,
                        'requerido' => true])

                        @include('componentes.input',['name'=>'email',
                        'nombreMostrar'=>'E-Mail:',
                        'editar'=>$usuario->email,
                        'requerido' => true])

                        @include('componentes.input',['name'=>'cuil',
                        'nombreMostrar'=>'CUIL:',
                        'editar'=>$usuario->cuil,
                        'requerido' => false])

                        @include('componentes.input',['name'=>'cuit',
                        'nombreMostrar'=>'CUIT:',
                        'editar'=>$usuario->cuit,
                        'requerido' => false])

                        @include('componentes.input',['name'=>'telefono',
                        'nombreMostrar'=>'Telefono:',
                        'editar'=>$usuario->telefono,
                        'requerido' => false])


                        @include('componentes.input',['name'=>'direccion',
                        'nombreMostrar'=>'Dirección:',
                        'editar'=>$usuario->direccion,
                        'clases'=>['autocomplete'],
                        'requerido' => false])

                        @include('componentes.input',['name'=>'password',
                        'nombreMostrar'=>'Contraseña:',
                        'editar'=>$usuario->password,
                        'tipo' => "password",
                        'requerido' => true])

                        @include('componentes.input',['name'=>'password_confirmation',
                        'nombreMostrar'=>'Confirmar Contraseña:',
                        'editar'=>$usuario->password,
                        'tipo' => "password",
                        'requerido' => true])

                        @include('componentes.check',['name'=>'superadmin',
                                             'nombreMostrar'=>'Es Admin:',
                                             'labelMostrar'=>'Es Admin:',
                                             'editar'=>$usuario->superadmin ])

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    {{$submitNombre}}
                                </button>
                            </div>
                        </div>
                    </form>

                    </div>
                 </div>
            </div>
        </div>
    </div>
</div>
