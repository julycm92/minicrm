<script>
    $(document).ready(function() {
        var table = $('#{{$tabla}}').DataTable({
            responsive: {
                details: {
                    renderer: function (api, rowIdx, columns) {
                        var data = $.map(columns, function (col, i) {
                            return col.hidden ?
                                '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                                '<td class="colTitleCollapse">' + col.title + ':' + '</td> ' +
                                '<td class="colDataCollapse">' + col.data + '</td>' +
                                '</tr>' :
                                '';
                        }).join('');

                        return data ?
                            $('<table/>').append(data) :
                            false;
                    }
                }
            },
            "processing": true,
            "serverSide": true,
            "ajax":{
                url :"{{route($dir)}}", // json datasource
                type: "post",  // method  , by default get
                data:{
                    _token: '{{csrf_token()}}'
                },
                error: function(){  // error handling
                    $(".employee-grid-error").html("");
                    $("#employee-grid").append('<tbody class="employee-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
                    $("#employee-grid_processing").css("display","none");

                }
            },
            "columns": [
                @foreach($nombres as $nombre)
                { "name": "{{$nombre}}",
                @isset($render[$nombre])
                  "render": {{$render[$nombre]}},
                @endisset
                @isset($ordenable[$nombre])
                  "orderable": {{$ordenable[$nombre]}},
                @endisset
                @isset($clase[$nombre])
                    "className": "{{$clase[$nombre]}}",
                @endisset
                @isset($tam[$nombre])
                "width": "{{$tam[$nombre]}}",
                @endisset
                },
                @endforeach
            ],
            @isset($orden) "order": [[ {{$orden['col']}}, '{{$orden['forma']}}' ]],@endisset
            "language":{
                "url":"{{asset('js/espDataTable.json')}}"
            },
        });

        $('#{{$tabla}} #inputsBuscar th').each( function (a) {
            $(this).html( '<input id="inputBuscar'+a+'" type="text"/>' );

            $('#inputBuscar'+a).on( 'keyup', function () {
                table
                    .columns( a )
                    .search( this.value )
                    .draw();
            } );
        } );

        table.on( 'responsive-resize', function ( e, datatable, columns ) {

            columns.forEach(function(elem,i){
                var head = 'th #inputBuscar'.concat(i);
                $(head).parent().css('display',elem ? '' : 'none');
            });
        });
    });
</script>