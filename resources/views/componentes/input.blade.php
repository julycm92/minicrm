<div class="form-group {{$errors->has($name) ? ' has-error' : ''}}">
    <label class="col-md-4 control-label" for="{{$name}}">{{$nombreMostrar}}</label>
    <div class="col-md-6">
        <input type="{{isset($tipo) ? $tipo : 'text'}}" class="form-control
            @isset($clases)
                @foreach($clases as $clase)
                    {{$clase}}
                @endforeach
            @endisset"
           id="{{$name}}" name="{{$name}}"
           value="{{old($name,isset($editar) ? $editar : '')}}"
            @isset($atributos)
                @foreach($atributos as $atributo=>$valor)
                    {{$atributo}}="{{$valor}}"
                @endforeach
            @endisset
            @isset($requerido) @if($requerido) required @endif @endisset>
    </div>
    @if ($errors->has($name))
        <span class="help-block">
            <strong>{{ $errors->first($name) }}</strong>
        </span>
    @endif
</div>