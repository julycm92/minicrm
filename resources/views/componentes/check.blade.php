<div class="pad-mar-limpios">
    <label class="col-md-4 control-label">{{$nombreMostrar}}</label>
    <div class="col-md-6 onoffswitch">
        <input style="margin-top:1%;" type="checkbox" class="checkbox"
               id="{{$name}}" name="{{$name}}" {{(isset($editar) & $editar) ? 'checked' : ''}}
        @isset($atributos)
            @foreach($atributos as $atributo=>$valor)
                {{$atributo}}="{{$valor}}"
            @endforeach
        @endisset
        @isset($requerido)
            @if($requerido)
                required
            @endif
        @endisset
        >
        <label class="onoffswitch-label" for="{{$name}}">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
    </div>
    @if ($errors->has($name))
        <span class="help-block">
            <strong>{{ $errors->first($name) }}</strong>
        </span>
    @endif
</div>
<div></div>
<br>