<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MigracionInicial extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('landings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('usuario_id')->unsigned()->index();
            $table->string('name');
            $table->string('dominio');
            $table->integer('cantidad_visitas');
        });

        Schema::create('clientes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('empresa_id')->unsigned()->index();
            $table->integer('landing_id')->unsigned()->index();
            $table->string('nombre');
            $table->string('email');
        });

        Schema::create('empresas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('razon_social');
            $table->string('dominio');
            $table->string('cuit');
            $table->string('telefono');
            $table->string('email');
        });

        Schema::table('landings', function (Blueprint $table) {
            $table->foreign('usuario_id')->references('id')->on('users');
        });

        Schema::table('clientes', function (Blueprint $table) {
            $table->foreign('landing_id')->references('id')->on('landings');
            $table->foreign('empresa_id')->references('id')->on('empresas');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
