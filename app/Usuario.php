<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Usuario extends Authenticatable
{

    protected $table = 'Users';
    protected $fillable = [
        'name', 'password', 'usuario', 'superadmin', 'empresa_id','email', 'direccion'
    ];


    protected $hidden = [
        'password', 'remember_token',
    ];
    public $timestamps = true;
}
