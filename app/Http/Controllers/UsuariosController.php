<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Usuario;
use Validator;

class UsuariosController extends Controller
{
    use SoftDeletes;

    public function index()
    {
        $usuarios = Usuario::all();
        return view('usuarios.listar', ['usuarios' => $usuarios]);
    }

    public function create()
    {

        return view('usuarios.create');
    }

    public function show()
    {
    }

    public function edit($id)
    {
        $usuario = Usuario::find($id);
        if ($usuario == null)
            return $this->error();
        return view('usuarios.edit', ['usuario' => $usuario]);
    }

    public function store(Request $request)
    {//dd($request);

        // Valido el input
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'telefono' => 'regex:[^\d*$]|max:25',
            'cuit' => 'regex:[^\d*$]|max:25',
            'cuil' => 'regex:[^\d*$]|max:25',
            'direccion' => 'max:255',

        ]);

        if ($validator->fails())
            return redirect('usuarios/create')->withErrors($validator)->withInput();

        Usuario::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'telefono' => $request['telefono'],
            'cuit' => $request['cuit'],
            'cuil' => $request['cuil'],
            'direccion' => $request['direccion'],
            'password' => bcrypt($request['password']),
        ]);
    }

    public function update()
    {
    }


}
