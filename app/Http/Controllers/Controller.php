<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    private function error()
    {
        return view('error', ['titulo' => 'Registrado no encontrado', 'mensaje' => 'No se encontró el registrado que intenta editar o no tiene permisos.']);
    }
}
